# Pillow dans un IDE

_Version actuelle de pyodide : {{ config.plugins.pyodide_macros.version }}_

## Test

{{ IDE('test_pil') }}

<div id="textfield">L'image originale</div>
<div id="original_image"></div>
<div id="textfield">L'image convertie</div>
<div id="new_image"></div>